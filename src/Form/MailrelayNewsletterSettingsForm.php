<?php

namespace Drupal\mailrelay_newsletter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Configure book settings for this site.
 *
 * @internal
 */
class MailrelayNewsletterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailrelay_newsletter_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailrelay_newsletter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mailrelay_newsletter.settings');
    $form['mailrelay_newsletter_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MailRelay ApiKey'),
      '#default_value' => $config->get('mailrelay_newsletter_apikey'),
      '#description' => $this->t('Your MailRelay ApiKey'),
      '#required' => TRUE,
    ];
    $form['mailrelay_newsletter_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MailRelay Hostname'),
      '#default_value' => $config->get('mailrelay_newsletter_hostname'),
      '#description' => $this->t('Your MailRelay Hostname'),
      '#required' => TRUE,
    ];

    $form['mailrelay_newsletter_sender_id'] = [
      '#type' => 'number',
      '#title' => $this->t('MailRelay Sender ID'),
      '#default_value' => $config->get('mailrelay_newsletter_sender_id') ? $config->get('mailrelay_newsletter_sender_id') : 1,
      '#min' => 1,
      '#description' => $this->t('Your MailRelay Sender ID'),
      '#required' => TRUE,
    ];

    $form['mailrelay_newsletter_header'] = array(
      '#type' => 'text_format',
      '#title' => 'Mail Header',
      '#format' => 'full_html',
      '#default_value' => $config->get('mailrelay_newsletter_header') ? $config->get('mailrelay_newsletter_header') : '<h1 style="text-align: center;">Mail header</h1>',
    );

    $form['mailrelay_newsletter_footer'] = array(
      '#type' => 'text_format',
      '#title' => 'Mail Footer',
      '#format' => 'full_html',
      '#default_value' => $config->get('mailrelay_newsletter_footer') ? $config->get('mailrelay_newsletter_footer') : '<p style="text-align: center;">Mail footer</p>',
    );

    $entity = Node::load($config->get('mailrelay_newsletter_privacy_policy_url'));
    $form['mailrelay_newsletter_privacy_policy_url'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Privacy Policy node'),
      '#default_value' => $entity,
      '#target_type' => 'node',
      '#selection_handler' => 'default',
      '#description' => $this->t('Your Privacy Policy page url.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $apikey = $form_state->getValue('mailrelay_newsletter_apikey');
    if (!$form_state->isValueEmpty(['mailrelay_newsletter_apikey', $apikey])) {
      $form_state->setErrorByName('mailrelay_newsletter_apikey', $this->t('The MailRelay ApiKey is required.'));
    }

    $hostname = $form_state->getValue('mailrelay_newsletter_hostname');
    if (!$form_state->isValueEmpty(['mailrelay_newsletter_hostname', $hostname])) {
      $form_state->setErrorByName('mailrelay_newsletter_hostname', $this->t('The MailRelay Hostname is required.'));
    }

    $domain = '.ipzmarketing.com';
    $old_domain = '.ip-zone.com';
    if (substr($form_state->getValue('mailrelay_newsletter_hostname'), -17) != $domain &&
      substr($form_state->getValue('mailrelay_newsletter_hostname'), -12) != $old_domain) {
      $form_state->setErrorByName('mailrelay_newsletter_hostname', $this->t('The MailRelay Hostname is not valid.'));
    }

    $sender_id = $form_state->getValue('mailrelay_newsletter_sender_id');
    if (!is_numeric($sender_id) || $sender_id === 0) {
      $form_state->setErrorByName('mailrelay_newsletter_sender_id', $this->t('The MailRelay Sender ID is not valid.'));
    }

    $gdpr_link = $form_state->getValue('mailrelay_newsletter_privacy_policy_url');
    if (!strpos( $gdpr_link, "http://") && !strpos( $gdpr_link, "https://") && !$gdpr_link[0] === "/") {
      $form_state->setErrorByName('mailrelay_newsletter_privacy_policy_url', $this->t('Please enter a valid url, the url must start by http://, https:// or /'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mailrelay_newsletter.settings')
      ->set('mailrelay_newsletter_apikey', $form_state->getValue('mailrelay_newsletter_apikey'))
      ->set('mailrelay_newsletter_hostname', $form_state->getValue('mailrelay_newsletter_hostname'))
      ->set('mailrelay_newsletter_sender_id', $form_state->getValue('mailrelay_newsletter_sender_id'))
      ->set('mailrelay_newsletter_header', $form_state->getValue('mailrelay_newsletter_header')['value'])
      ->set('mailrelay_newsletter_footer', $form_state->getValue('mailrelay_newsletter_footer')['value'])
      ->set('mailrelay_newsletter_privacy_policy_url', $form_state->getValue('mailrelay_newsletter_privacy_policy_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
