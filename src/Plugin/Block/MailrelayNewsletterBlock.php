<?php

namespace Drupal\mailrelay_newsletter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\mailrelay_newsletter\MailrelayServiceInterface;

/**
 * Provides a 'MailrelayNewsletterBlock' block.
 *
 * @Block(
 *  id = "mailrelay_newsletter_block",
 *  admin_label = @Translation("Mailrelay newsletter block"),
 * )
 */
class MailrelayNewsletterBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Mailrelay service.
   *
   * @var \Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\mailrelay_newsletter\MailrelayServiceInterface
   */
  protected $mailrelayService;

  /**
   * The Form Builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new MailrelayNewsletterBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\mailrelay_newsletter\MailrelayServiceInterface $mailrelay_service
   *   The form builder.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FormBuilderInterface $form_builder,
    MailrelayServiceInterface $mailrelay_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailrelayService = $mailrelay_service;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('mailrelay.main')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $categories = $this->mailrelayService->getGroups();
    $options = [];
    foreach ($categories as $category) {
      $options[$category['id']] = $category['name'];
    }

    $form['mailrelay_newsletter_subtitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subtitle'),
      '#default_value' => $this->configuration['mailrelay_newsletter_subtitle'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];

    $form['mailrelay_newsletter_visible_categories'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Visible Categories'),
      '#options' => $options,
      '#default_value' => $this->configuration['mailrelay_newsletter_visible_categories'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['mailrelay_newsletter_subtitle'] = $form_state->getValue('mailrelay_newsletter_subtitle');
    $this->configuration['mailrelay_newsletter_visible_categories'] = $form_state->getValue('mailrelay_newsletter_visible_categories');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('\Drupal\mailrelay_newsletter\Form\NewsletterForm', $this->configuration);
    return [
      '#theme' => 'mailrelay_newsletter_form',
      '#subtitle' => $this->configuration['mailrelay_newsletter_subtitle'],
      '#form' => $form,
    ];
  }

}
