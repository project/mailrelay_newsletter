<?php

namespace Drupal\mailrelay_newsletter;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;

/**
 * Provides handling to MailRelay.
 */
class MailrelayService implements MailrelayServiceInterface {

  /**
   * Old vars.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Old vars.
   *
   * @var string
   */
  protected $apiEndpoint = 'https://<dc>/api/v1/';

  /**
   * Old vars.
   *
   * @var bool
   */
  protected $requestSuccessful = FALSE;

  /**
   * Old vars.
   *
   * @var string
   */
  protected $lastError = '';

  /**
   * Old vars.
   *
   * @var array
   */
  protected $lastResponse = [];

  /**
   * Old vars.
   *
   * @var array
   */
  protected $lastRequest = [];

  /**
   * Old vars.
   *
   * @var array
   */
  protected $client = [];

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, Client $http_client) {
    $this->config = $config_factory->get('mailrelay_newsletter.settings');
    $this->apiKey = $this->config->get('mailrelay_newsletter_apikey');
    $this->apiEndpoint = str_replace('<dc>', $this->config->get('mailrelay_newsletter_hostname'), $this->apiEndpoint);
    $this->lastResponse = ['headers' => NULL, 'body' => NULL];
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function __call(string $function, $arguments) {
    list($method, $args, $router_parameters) = $arguments;
    // If Allowed.
    $allowed_funcs = [
      'ab_tests',
      'campaigns',
      'campaigns_folders',
      'custom_fields',
      'imports',
      'groups',
      'media_files',
      'media_folders',
      'rss_campaigns',
      'senders',
      'sent_campaigns',
      'smtp_emails',
      'smtp_tags',
      'subscribers',
      'unsubscribe_events',
      'api_batches',
    ];

    if (in_array($function, $allowed_funcs)) {
      $args = !empty($args) ? $args : $args = [];
      try {
        return $this->makeRequestHttp($method, $function, $args, $router_parameters);
      }
      catch (\Exception $e) {
      }
    }
    if (method_exists($this, $function)) {
      return call_user_func_array(
        [$this, $function],
        array($function, $args));
    }
    trigger_error("Call to undefined function '{$function}'");
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function success() {
    return $this->requestSuccessful;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastError() {
    return $this->lastError ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastResponse() {
    return $this->lastResponse;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastRequest() {
    return $this->lastRequest;
  }

  /**
   * {@inheritdoc}
   */
  public function makeRequestHttp($method, $function, array $args = [], array $route_parameters = []) {
    $params = [];
    $headers = [
      'x-auth-token' => $this->apiKey,
      'content-type' => 'application/json',
      'accept' => 'application/json',
    ];

    if (!empty($args)) {
      foreach ($args as $key => $arg) {
        $params[$key] = $arg;
      }
    }

    $callback = $this->apiEndpoint . $function;
    if (!empty($route_parameters) && isset($route_parameters[0])) {
      $callback = $this->apiEndpoint . $function . '/' . implode('/', $route_parameters);
    }
    try {
      $response = $this->httpClient->$method($callback, [
        'headers' => $headers,
        'body' => Json::encode($params),
      ]);

      if ($this->determineSuccess($response)) {
        return $this->formatResponse($response);
      }

      return $this->getLastError();

    }
    catch (ClientException $e) {
      $responseBody = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
      $this->lastError = $responseBody['errors'];
      return $responseBody;
    }
  }

  /**
   * Decode the response and format any error messages for debugging.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The response from the curl request.
   *
   * @return \GuzzleHttp\Psr7\Response|\bool
   *   The JSON decoded into an array.
   */
  protected function formatResponse(Response $response) {
    $this->lastResponse = $response;
    if (!empty($response->getBody()->getContents())) {
      return json_decode($response->getBody(), TRUE);
    }
    return FALSE;
  }

  /**
   * Check if the response was successful or a failure. Stored if error.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The response from the curl request.
   *
   * @return bool
   *   If the request was successful.
   */
  protected function determineSuccess(Response $response) {
    $status = $this->findHttpStatus($response);
    if ($status >= 200 && $status <= 299) {
      $this->requestSuccessful = TRUE;
      return TRUE;
    }
    $responseBody = json_decode($response->getBody()->getContents(), TRUE);
    $this->lastError = $responseBody['errors'];
    return FALSE;
  }

  /**
   * Find the HTTP status code from the headers or API response body.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The response from the curl request.
   *
   * @return int
   *   HTTP status code.
   */
  protected function findHttpStatus(Response $response) {
    return $response->getStatusCode();
  }

  /**
   * {@inheritdoc}
   */
  public function getGroups() {
    $groups =  $this->groups('GET', [], []);
    if ($this->getLastError()) {
      return FALSE;
    }
    return $groups;
  }

}
