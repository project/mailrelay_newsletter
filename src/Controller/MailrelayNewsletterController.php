<?php

namespace Drupal\mailrelay_newsletter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mailrelay_newsletter\MailrelayServiceInterface;

/**
 * Controller routines for MailRelay routes.
 */
class MailrelayNewsletterController extends ControllerBase {

  protected $mailrelayService;

  /**
   * MailrelayNewsletterController constructor.
   *
   * @param \Drupal\mailrelay_newsletter\MailrelayServiceInterface $service
   *   the MailRelayService interface.
   */
  public function __construct(MailrelayServiceInterface $service) {
    $this->mailrelayService = $service;
  }

  /**
   * Create function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface.
   *
   * @return \Drupal\Core\Controller\ControllerBase|\Drupal\mailrelay_newsletter\Controller\MailrelayNewsletterController
   *   The MailRelayNewsletter controller.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mailrelay.main')
    );
  }

  /**
   * Debug function only for testing pourposes.
   */
  public function debug() {
    $this->mailrelayService->debug();
  }

}
