<?php

namespace Drupal\mailrelay_newsletter;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;

/**
 * Defines an interface for an MailrelayService service.
 */
interface MailrelayServiceInterface {

  /**
   * Create a new instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\Client $http_client
   *   An HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Client $http_client);

  /**
   * Function that call a method.
   *
   * @param string $function
   *   The function name.
   * @param mixed $arguments
   *   The arguments to pass to the call.
   *
   * @return mixed
   *   The result of the call.
   */
  public function __call(string $function, $arguments);

  /**
   * Was the last request successful?
   *
   * @return bool
   *   True for success, false for failure.
   */
  public function success();

  /**
   * Get the last error returned by either the network transport, or by the API.
   *
   * @return mixed|false
   *   Describing the error.
   */
  public function getLastError();

  /**
   * Get an array containing the HTTP headers and the body of the API response.
   *
   * @return array
   *   Assoc array with keys 'headers' and 'body'.
   */
  public function getLastResponse();

  /**
   * Get an array containing the HTTP headers and the body of the API request.
   *
   * @return array
   *   Assoc array.
   */
  public function getLastRequest();

  /**
   * Performs the underlying HTTP request. Not very exciting.
   *
   * @param string $method
   *   The API method to be called.
   * @param string $function
   *   The API function to be called.
   * @param array $args
   *   Assoc array of parameters to be passed.
   * @param array $route_parameters
   *   Assoc array of route parameters to be passed.
   *
   * @return array|false
   *   Assoc array of decoded result.
   *
   * @throws \Exception
   */
  public function makeRequestHttp($method, $function, array $args = [], array $route_parameters = []);

  /**
   * Get all groups in Mailrelay.
   *
   * @return array|bool
   *   The groups array or FALSE.
   */
  public function getGroups();

}
