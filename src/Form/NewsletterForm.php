<?php

namespace Drupal\mailrelay_newsletter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\mailrelay_newsletter\MailrelayServiceInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class NewsletterForm.
 *
 * @package Drupal\mailrelay_newsletter\Form
 */
class NewsletterForm extends FormBase implements ContainerInjectionInterface {

  protected $mailrelayService;

  protected $categories;

  protected $messenger;

  protected $configFactory;

  protected $visibleCategories;

  /**
   * The interface language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $interfaceLanguage;

  /**
   * MailrelayNewsletterController constructor.
   *
   * @param \Drupal\mailrelay_newsletter\MailrelayServiceInterface $service
   *   The MailRelayService interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(MailrelayServiceInterface $service,
                              MessengerInterface $messenger,
                              ConfigFactoryInterface $config_factory,
                              LanguageManagerInterface $language_manager) {
    $this->mailrelayService = $service;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
    $this->interfaceLanguage = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mailrelay.main'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsletter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config = NULL) {
    $selected_categories = array_filter($config['mailrelay_newsletter_visible_categories'], function ($a) {
      return ($a !== 0);
    });
    $selected_categories = array_keys($selected_categories);
    $this->visibleCategories = $selected_categories;

    $this->categories = $this->mailrelayService->getGroups();
    $options = [];
    if (!empty($this->categories)) {
      foreach ($this->categories as $category) {
        if (in_array($category['id'], $selected_categories)) {
          $options[$category['id']] = $category['name'];
        }
      }
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
    ];
    $form['newsletter_subscribe_option'] = [
      '#type' => 'radios',
      '#default_value' => 'all',
      '#options' => [
        'all' => $this
          ->t('Subscribe to all categories'),
        'only_selected' => $this
          ->t('Subscribe to specified categories'),
      ],
      '#required' => TRUE,
    ];
    $form['newsletter_categories'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Categories'),
      '#options' => $options,
      '#states' => [
        'visible' => [
          'input[name="newsletter_subscribe_option"' => ['value' => 'only_selected'],
        ],
        'required' => [
          'input[name="newsletter_subscribe_option"' => ['value' => 'only_selected'],
        ],
      ],
    ];
    $privacy_node = $this->configFactory->get('mailrelay_newsletter.settings')->get('mailrelay_newsletter_privacy_policy_url');
    if(!empty($privacy_node)) {
      $privacy_node = Node::load($privacy_node);
      $node_url = $privacy_node->toUrl('canonical', ['language' => $this->interfaceLanguage->getCurrentLanguage()])
        ->toString();
    }

    $form['gdpr_acceptance'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('I have read and accept the <a href="@link" target="_blank">Privacy Policy</a>', ['@link' => $node_url]),
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Subscribe'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_categories = [];

    if ($form_state->getValue('newsletter_subscribe_option') === 'only_selected') {
      $categories = $form_state->getValue('newsletter_categories');
      foreach ($categories as $key => $category) {
        if ($category != 0) {
          array_push($selected_categories, intval($category));
        }
      }
    }
    else {
      $selected_categories = $this->visibleCategories;
    }
    $arguments = [
      'status' => 'active',
      'email' => $form_state->getValue('email'),
      'group_ids' => $selected_categories,
    ];

    $deleted_subscribers = $this->mailrelayService->subscribers('GET', [], ['deleted?q[email_eq]='.$form_state->getValue('email')]);
    if (isset($deleted_subscribers[0]['email']) && $form_state->getValue('email') === $deleted_subscribers[0]['email']) {
      // Restore the subscriber from deleted.
      $restore_args = [
        'id' => $deleted_subscribers[0]['id'],
      ];
      $restore = $this->mailrelayService->subscribers('PATCH', $restore_args, [$deleted_subscribers[0]['id'], 'restore']);
      $update_args = [
        'id' => $deleted_subscribers[0]['id'],
        'email' => $deleted_subscribers[0]['email'],
        'group_ids' => $selected_categories,
      ];
      $udpated_subscriber = $this->mailrelayService->subscribers('PATCH', $update_args, [$deleted_subscribers[0]['id']]);
    }
    else {
      $subscriber = $this->mailrelayService->subscribers('POST', $arguments, []);
    }

    $lastError = $this->mailrelayService->getLastError();

    if ($lastError) {
      foreach ($lastError as $key => $error) {
        $this->messenger()->addError($error[0]);
      }
    }
    else {
      $this->messenger()->addStatus(t('Email subscribe success'));
    }
  }

}
